var UI = (function(my) {

	ago.click(function() {

		if (castling == interval) {
			interval--;
		}

		if (interval > 0) {
			interval--;
			console.log(interval);

			time_travel(interval);
		} else {
			console.log('ago end');
		}

	});

	forward.click(function() {

		if (interval < castling - 1) {
			interval++;
			console.log(interval);

			time_travel(interval);
		} else {
			console.log('ago end');
		}

	});

	quantity_plus.click(function() {

		if (quantity < 10) {
			quantity++;
			quantity_panel.html(quantity);
			New_particle(particle_array, quantity - 1);
		} else {
			error_block.html('Большое количество частиц может сильно нагрузить ваш ПК!');
		}

	});

	quantity_minus.click(function() {

		if (quantity > 1) {
			quantity--;
			quantity_panel.html(quantity);
			particle_array[quantity] = '';
			$('.' + quantity).detach();
		} else {
			error_block.html('Нельзя удолять последнюю частицу!');
		}

	});

	function time_travel(interval) {

		for (var i = 0; i < quantity; i++) {

			x = chronology[interval].interval[i].position_x;
			y = chronology[interval].interval[i].position_y;
			v = chronology[interval].interval[i].speed;
			t = chronology[interval].interval[i].f_time;

			particle_array[i] = {
				numb: i,
				position_x: x,
				position_y: y,
				gravitation: g,
				speed: v,
				f_time: t
			};

			$('.' + i).css({
				'left': x,
				'bottom': y
			});

		}
	}

	return (my);
}(UI || {}))
