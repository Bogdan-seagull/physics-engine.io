var error_block = $('#Error_block');

var space = $('#Physical_space');

var ago = $('#step_ago');
var togglе = $('#step_togglе');
var forward = $('#step_forward');

var quantity_plus = $('#quantity_plus');
var quantity_panel = $('#quantity');
var quantity_minus = $('#quantity_minus');

var width_space = space.width();
var height_space = space.height();

var G = 6.67;
var g = 9.8 / 1;
var quantity = 2;
var fps = 10000 / 60;
var $flag = true;

var x, y, start;

var m = 0,
	r = 0,
	v = 0,
	t = 0,
	afloat = 0,
	interval = 0,
	castling = 0;

var particle_array = {};
var chronology = {};
var interval_array = [];

include('_GRAVITY');
include('_MOMENTUM');
include('_UI');

quantity_panel.html(quantity);

for (var i = 0; i < quantity; i++) {
	New_particle(particle_array, i);
}

function New_particle(particle_array, i) {

	x = Math.floor(Math.random() * (width_space - 0) + 0);
	y = Math.floor(Math.random() * (height_space - 0) + 0);

	particle_array[i] = {
		numb: i,
		position_x: x,
		position_y: y,
		gravitation: g,
		speed: v,
		f_time: t
	};

	space.append('<div class="Point ' + i + '"></div>');

	$('.' + i).css({
		'left': x,
		'bottom': y
	});

	return particle_array;

}

function include(url) {
	$.getScript('/js/' + url + '.js');
}
