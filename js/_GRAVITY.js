var GRAVITY = (function(my) {

	togglе.click(function() {

		if ($flag == true) {

			$flag = false;

			start = setInterval(function() {

					interval_array = [];

					for (var i = 0; i < quantity; i++) {

						x = particle_array[i].position_x;
						y = particle_array[i].position_y;
						g = particle_array[i].gravitation;
						v = particle_array[i].speed;
						t = particle_array[i].f_time;

						if (y > 0 && y < height_space) {
							t = Math.sqrt((2 * y) / g);
							v = v + g * t ^ 2;
							y = y - v / t ^ 2;
							if (y < 0) {
								y = 0;
								afloat++;
							}
						} else {
							y = 0;
						}

						particle_array[i] = {
							numb: i,
							position_x: x,
							position_y: y,
							gravitation: g,
							speed: v,
							f_time: t
						};

						$('.' + i).css({
							'left': x,
							'bottom': y
						});

						interval_array.push(particle_array[i]);
						chronology[interval] = {
							interval: interval_array
						};

						if (afloat === quantity) {
							console.log(chronology);
							clearInterval(start);
						}

					}

					interval++;
					castling = interval;

				},
				fps);

		} else {
			$flag = true;
			clearInterval(start);
		}

	});

	return my;
}(GRAVITY || {}))
