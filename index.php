<?php
$site_url = "http://".$_SERVER['SERVER_NAME'];
?>

<!DOCTYPE html>
<html lang="ru" hreflang="uk">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9" hreflang="uk">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>PhysicsEngine</title>

		<link rel="stylesheet" href="<? echo $site_url; ?>/css/style.css" >

		<script src="<? echo $site_url; ?>/js/jquery.js"></script>

	</head>

	<body>

		<div id="Error_block"></div>

		<div id="Physical_space"></div>

		<div id="Engine_panel">

			<div id="control_panel">
				<a href="#" id="step_ago" class="btn_control icon icon-previous2 "></a>
				<a href="#" id="step_togglе" class="btn_control icon icon-pause2"></a>
				<a href="#" id="step_forward" class="btn_control icon icon-next2"></a>
			</div>

			<div id="quantity_panel">
				<a href="#" id="quantity_plus" class="btn_control icon icon-plus "></a>
				<div id="quantity"></div>
				<a href="#" id="quantity_minus" class="btn_control icon icon-minus"></a>
			</div>

		</div>

	</body>

	<script src="<? echo $site_url; ?>/js/main.js"></script>

</html>
